'''
Mail Fetching Program from Subject line 

@author: Jinesh Patel

'''


import imaplib,email,re,time,datetime,sys



class EmailConnection():
    
    total_email = 0
    subject_matched = 0
    
    def __init__(self,usrname,passwd):
        
        self.imapsrvr = imaplib.IMAP4_SSL("imap.gmail.com",993)
        self.imapsrvr.login(usrname,passwd)
        
    def DataManipulation(self,iter):
        self.inbox = self.imapsrvr.select('INBOX')
        self.result,self.data = self.imapsrvr.search(None,"ALL")
        self.email_uid = self.data[0].split()
        self.result,self.data = self.imapsrvr.fetch(self.email_uid[-1-iter],"(RFC822)")
        self.raw_email = self.data [0][1]
        self.email_msg = email.message_from_string(self.raw_email)
        self.rcv = self.email_msg['Received']                        # Fetching received Field Data From Email
        self.subject = self.email_msg['Subject'].decode('UTF-8')     # Fetching Subject From E-mail 
#        print self.subject
        
    
    def DateManipulation(self,END_DATE,BEG_DATE,subject):
        '''Note: When you Create This Object Pass How many days old e-mail you want to check 
        and what is the subject line (you can also pass part of the subject as a string) 
        '''
        self.date_obj = re.compile(r'\d+\d+\s+\w+\s+\d+')            #Getting Date From Email Object
        self.r_date = re.findall(self.date_obj,self.rcv )            
        self.rec_date = datetime.datetime.strptime(str(self.r_date[0]),"%d %b %Y").date()
#        print self.rec_date
#        self.currentTime = (datetime.date.today().strftime("%d %b %Y"))
#        print  datetime.date.today()
#        self.delta2 = 
        self.delta1 = self.rec_date -  datetime.datetime.strptime(END_DATE,"%d-%b-%Y").date()
        self.delta2 = self.rec_date -  datetime.datetime.strptime(BEG_DATE,"%d-%b-%Y").date()
        a=  str(self.delta1.days)
        b =  str(self.delta2.days)
        if(int(a) == 0 and int(b) >= 0):
            if subject in self.subject:
                EmailConnection.subject_matched += 1
                print "Total matched Subjects are %s"%EmailConnection.subject_matched   
#                print self.subject
            else:
 #               sys.stdout.write("No Subject Match Found\n\n\n")
                return EmailConnection.subject_matched
        elif(int(a) < 0 and int(b) < 0):
            sys.exit()
            





if __name__ == '__main__':
    
    if len(sys.argv) < 4:
        sys.stderr.write("Usage: python " + sys.argv[0] +" <END-DATE> <BEG-DATE> <subject> DATE FORMAT : dd-MON-YYYY(23-FEB-2014)")
        exit()
    else:
        e = EmailConnection('jinesh93','xxxyyyzzz')   # put your UserName and Password Here
        for i in range(100000):
            e.DataManipulation(i)
            e.DateManipulation(sys.argv[1],sys.argv[2],sys.argv[3])
            
